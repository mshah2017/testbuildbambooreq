package TestMavenSelenium;

import java.io.IOException;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;



public class TestMavenSelenium {
	WebDriver driver;
	@BeforeMethod
	public void beforeMethod()
	{
		System.setProperty("webdriver.chrome.driver","C:\\Automation\\Open2Test_Selenium-master\\SampleScript\\Driver\\chromedriver.exe"); 
		 driver = new ChromeDriver();
		 driver.manage().window().maximize();
		 return;
	}
		@AfterMethod
		public void afterMethod() {
		  //System.out.println("method name:" + result.getMethod().getMethodName());
		//close browser
		driver.quit();
		
	}
		
}